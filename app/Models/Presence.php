<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Presence extends Model
{
    use HasFactory;

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    protected $casts = [
        'metadata' => 'array',
        'latidude' => 'float',
        'longitude'=> 'float',

    ];
}
